import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class filecopy{
	
	public void write(String filename, String line){
		
		try {


			File file = new File(filename);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.append(line);
			bw.newLine();
			bw.close();


		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

