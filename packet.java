import java.nio.ByteBuffer;

public class packet{
	
	public int seqnum = 1;  //4bytes
	public int RC = 2;   //4 bytes
	public long time = 1234567890123L;  
	public String data = "default";
	public int pktsize = 1300; 
	
	public packet(int seqnum, int RC, long time, String data){
		this.seqnum = seqnum;
		this.RC = RC;
		this.time = time;
		this.data = data;
		
	}
	
	public packet(int seqnum, int RC, long time, int pktsize){
		this.seqnum = seqnum;
		this.RC = RC;
		this.time = time;
		this.pktsize = pktsize;
		this.data = new String(new char[pktsize-16]).replace('\0', 's');

	}
	
	public packet(byte[] total){
		
		byte[] seqnumb = new byte[4];
		
		for(int i=0; i<4; i++){
            seqnumb[i] = total[i];
		}
		ByteBuffer wrapped = ByteBuffer.wrap(seqnumb); // big-endian by default
        this.seqnum = wrapped.getInt(); 
        
        byte[] RCb = new byte[4];
		
		for(int i=0; i<4; i++){
            RCb[i] = total[i+4];
		}
		ByteBuffer wrapped1 = ByteBuffer.wrap(RCb); // big-endian by default
        this.RC = wrapped1.getInt();
		
		byte[] timeb = new byte[8];
		
		for(int i=0; i<8; i++){
            timeb[i] = total[i+8];
		}        
        ByteBuffer wrapped2 = ByteBuffer.wrap(timeb); // big-endian by default
        this.time = wrapped2.getLong();
        
        byte[] datab = new byte[total.length - 16];
		
		for(int i=0; i<total.length - 16; i++){
            datab[i] = total[i + 16];
		}        
        this.data = new String(datab);
        
	}
	
	public packet(){
		
	}
	
	public void decRC(){
		this.RC--;
	}
	
	public String toString(){
		return ("Sequence Number:" + Integer.toString(this.seqnum) + " RC:" + Integer.toString(this.RC) + 
		        " time:" + Long.toString(this.time) + " data:" + this.data);
	}
	
	public int len(){
		return (16+ data.length());
	}
	
	public byte[] getByt(){
		
	    byte[] seqnumb = ByteBuffer.allocate(4).putInt(seqnum).array();
	    byte[] RCb = ByteBuffer.allocate(4).putInt(RC).array();
		byte[] timeb = ByteBuffer.allocate(8).putLong(time).array();
		byte[] datab = data.getBytes();
		
		//byte[] totalb = new byte[seqnumb.length + RCb.length + timeb.length + datab.length];
		byte[] totalb = new byte[this.pktsize];
		int start = 0;
		
		for(int i=0; i<seqnumb.length; i++){
			totalb[i + start] = seqnumb[i];
		}
		
		start += seqnumb.length;
		
		for(int i=0; i<RCb.length; i++){
			totalb[i + start] = RCb[i];
		}
		
		start += RCb.length;
		
		for(int i=0; i<timeb.length; i++){
			totalb[i + start] = timeb[i];
		}
		start += timeb.length;
		
		for(int i=0; i<datab.length; i++){
			totalb[i + start] = datab[i];
		}
		
		return totalb;
		
	}
	
	public int getseqnum(){
		return this.seqnum;
	}
	
	public int getRC(){
		return this.RC;
	}
	
	public long gettime(){
		return this.time;
	}
	
	public String getdata(){
		return this.data;
	}
	
	public void setseqnum(int seqnum){
		this.seqnum = seqnum;
	}
	
	public void setRC(int RC){
	    this.RC = RC;
	}
	
	public void settime(long time){
		this.time = time;
	}
	
	public void setdata(String data){
		this.data = data;
	}
  
}
