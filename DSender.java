import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class DSender{
	
  public InetAddress ip;
  public int port;
  
  public DSender(String ip, int port) throws Exception {
	  
	  this.ip = InetAddress.getByName(ip);
	  this.port = port;
  }
  
  public void sen(packet pkt, DatagramSocket ds) throws Exception {
	  
    //DatagramSocket ds = new DatagramSocket();
    
    
    DatagramPacket dp = new DatagramPacket(pkt.getByt(), pkt.len(), this.ip, this.port);
    ds.send(dp);
    
    //ds.close();
  }
  
  
}
