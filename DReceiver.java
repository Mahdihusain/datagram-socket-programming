import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class DReceiver{

  public int port;
  public int packetsize;
  
  public DReceiver(int port, int packetsize){
	  this.port = port;
	  this.packetsize = packetsize;
  }
  
  public packet rec(DatagramSocket ds) throws Exception {
   
    //DatagramSocket ds = new DatagramSocket(this.port);
    byte[] buf = new byte[this.packetsize];
    
    DatagramPacket dp = new DatagramPacket(buf, this.packetsize);
    //System.out.println("Receiving started");
    ds.receive(dp);
    //System.out.println("Receiving completed");
    packet pktrecv = new packet(dp.getData());
    
    //System.out.println(pktrecv.toString());
    //ds.close();
    return pktrecv;
    
  }
  
  public packet rec() throws Exception {
   
    DatagramSocket ds = new DatagramSocket(this.port);
    byte[] buf = new byte[this.packetsize];
    
    DatagramPacket dp = new DatagramPacket(buf, this.packetsize);
    ds.receive(dp);
    
    packet pktrecv = new packet(dp.getData());
    
    System.out.println(pktrecv.toString());
    ds.close();
    return pktrecv;
    
  }
  
}
