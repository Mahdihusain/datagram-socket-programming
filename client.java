//Never take port as 0000
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.io.*;
import java.net.DatagramSocket;
import java.util.concurrent.*;

public class client{
	
	public static packet pktc;
	
	public static void setPacket(packet p){
		client.pktc = p;
	};
	
	public static void main(String[] args) throws Exception{
		
		//client obj = new client(); 

	    Scanner scan = new Scanner(System.in); 
	    System.out.println("Enter the size of packets: ");
	    String Psizeofpkts = scan.nextLine();
	    System.out.println("Enter the RC value: ");
	    String TRCs = scan.nextLine();
	    System.out.println("Enter the IP of receipent: ");
	    String ips = scan.nextLine();
	    System.out.println("Enter the name of file: ");
	    String filename = scan.nextLine();
	    System.out.println("Enter timeout in ms: ");
	    String timeouts = scan.nextLine();
	    
	    int Psizeofpkt = Integer.parseInt(Psizeofpkts);
	    int TRC = Integer.parseInt(TRCs);
	    int timeout = Integer.parseInt(timeouts);
       
        DSender send = new DSender(ips, 1111);  //add ips here later
		filecopy fileobj = new filecopy();
		Timer timer = new Timer();
		
	    
	    packet pktobj = new packet(0, TRC, 0L , Psizeofpkt); 
	    DReceiver receive = new DReceiver(2222, Psizeofpkt);
	    DatagramSocket ds = new DatagramSocket();
		DatagramSocket dsr = new DatagramSocket(2222);

	    for(int i=0; i<50; i++){
		
	    long starttime = System.currentTimeMillis();
		
		pktobj.setRC(TRC);
		pktobj.setseqnum(i);
		pktobj.settime(starttime);
		
	    pktobj.decRC();
	    
	    setPacket(pktobj);
	    
	    ExecutorService service = Executors.newSingleThreadExecutor();
	    try {
            Runnable r = new Runnable() {
            @Override
            public void run() {
			    try{
					  boolean isRC = true;
					  int RC = TRC;
				      while(isRC){
						  if(RC>0){
			                  send.sen( client.pktc, ds);
                              setPacket(receive.rec(dsr));
                              client.pktc.decRC();
				              RC = client.pktc.getRC();
			              }else{
							  String time = Psizeofpkt + "," + Long.toString(System.currentTimeMillis() - starttime);
							  fileobj.write(filename, time);  //add filename here
							  //System.out.println(System.currentTimeMillis() - starttime);
							  isRC = false;
			              }
					
		              }
		        }catch(Exception e){
				    System.out.println("Error receiving");
			    }
            }
		    };

                Future<?> f = service.submit(r);

                f.get(timeout, TimeUnit.MILLISECONDS);     // add timeout later
            }catch (final InterruptedException e) {
				//System.out.println("Timeout1");
    // The thread was interrupted during sleep, wait or join
            }catch (final TimeoutException e) {
    // Took too long!
                System.out.println("Timeout");
            }catch (final ExecutionException e) {
				//System.out.println("Timeout3");
			}
            finally {
				//System.out.println("Timeout");
                service.shutdown();
            }
	    
	    
        }
   
        System.out.println("Process Over");
        System.exit(0);
    }
}
/*   
   public String addRC (int RC, String str){
	   return (Integer.toString(RC) + "_" + str);
   }
   
   public String decRC(String str){
		
		String strRC = "";
		int intRC = 0;
		
		for(int i=0; i<str.length(); i++){
			String sub = (str.substring(i, i+1));
			
			if(sub.equals("_")){
				
				intRC = Integer.parseInt(strRC);
				intRC -= 1;
				str = str.substring(i);
				str = Integer.toString(intRC) + str;
				RC = intRC;
				return str;
				
			}else{
				strRC += sub;
			}
		}
		return str;
	}
*/  

